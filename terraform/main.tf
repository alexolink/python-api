provider "aws" {
  region = "us-east-1"
}

module "vpc" {
  source = "terraform-aws-modules/vpc/aws"

  name = "peex-vpc"
  cidr = "10.0.0.0/16"

  azs             = ["us-east-1a", "us-east-1b", "us-east-1c"]
  private_subnets = ["10.0.1.0/24", "10.0.2.0/24", "10.0.3.0/24"]
  public_subnets  = ["10.0.101.0/24", "10.0.102.0/24", "10.0.103.0/24"]

  tags = {
    Terraform = "true"
    Environment = "test"
  }
}


module "gitlab_runner_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "GitLab Runner Security Group"
  description = "Security group for GitLab Runner instances"
  vpc_id      = module.vpc.vpc_id

#  ingress_cidr_blocks      = ["<insert your IP>"]
#  ingress_rules            = ["ssh-tcp"]

  egress_cidr_blocks       = ["0.0.0.0/0"]
  egress_rules             = ["all-all"] # Open all ports & protocols for egress traffic
}

module "deploy_sg" {
  source = "terraform-aws-modules/security-group/aws"

  name        = "GitLab Runner Security Group"
  description = "Security group for GitLab Runner instances"
  vpc_id      = module.vpc.vpc_id

#  ingress_cidr_blocks      = ["<insert your IP>"]
#  ingress_rules            = ["ssh-tcp"]
  ingress_with_cidr_blocks = [
    {
      rule        = "http-80-tcp"
      cidr_blocks = "0.0.0.0/0"
    }
  ]

  egress_cidr_blocks       = ["0.0.0.0/0"]
  egress_rules             = ["all-all"] # Open all ports & protocols for egress traffic
}


module "ec2_runner" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "gitlab-runner"

  ami                    = "ami-0b18dcd88401399d3"
  instance_type          = "t2.small"
  key_name               = "mac"
  vpc_security_group_ids = [module.gitlab_runner_sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]

  tags = {
    Terraform   = "true"
    Environment = "test"
  }
}

module "ec2_deploy" {
  source  = "terraform-aws-modules/ec2-instance/aws"
  version = "~> 3.0"

  name = "Instance-for-deploy"

  ami                    = "ami-0b18dcd88401399d3"
  instance_type          = "t2.small"
  key_name               = "mac"
  vpc_security_group_ids = [module.deploy_sg.security_group_id]
  subnet_id              = module.vpc.public_subnets[0]

  tags = {
    Terraform   = "true"
    Environment = "test"
  }
}
