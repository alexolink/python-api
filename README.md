## Python-api project
1. Run app
    ```bash
    pipenv --python 3.7
    pipenv install -r requirements.txt
    pipenv run python app.py
    ```
2. Run tests:
    ```bash
    pipenv run pytest test_app.py --url http://localhost:5000
    ```
